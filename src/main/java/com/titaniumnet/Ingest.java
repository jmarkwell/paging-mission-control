package com.titaniumnet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

/** 
 * paging-mission-control @author Jordan Markwell
 */

public class Ingest {
    
    public static void main(String[] args) throws IOException {
        BufferedReader input = null;
        
        try {
            InputStream stream = Ingest.class.getClassLoader().getResourceAsStream("input.txt");
            input = new BufferedReader( new InputStreamReader(stream, "UTF-8") );
        }
        catch (Exception FileNotFoundException) {
            System.err.println("failed to open input.txt");
            System.exit(1);
        }
        
        final int FIVE_MINUTES = 300000;
        
        DateTimeFormatter dateTimeFormat = DateTimeFormatter.ofPattern("uuuuMMdd HH:mm:ss.SSS").withZone( ZoneId.of("Z") );
        
        long latestBattTimestamp = 0;
        long middleBattTimestamp = 0;
        long oldestBattTimestamp = 0;
        
        long latestTstatTimestamp = 0;
        long middleTstatTimestamp = 0;
        long oldestTstatTimestamp = 0;
        
        int battAlerts = 0;
        int tstatAlerts = 0;
        
        String line;
        while( (line = input.readLine() ) != null ) {
            // DEBUG CODE
            //System.out.println(line);
            
            //      0     |       1      |        2       |         3         |         4        |       5       |     6     |     7
            // <timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
            List<String> telemetry = Arrays.asList( line.split("\\|") );
            // DEBUG CODE
            //System.out.println( telemetry.get(7) );
            
            // If battery telemetry meets the alert threshold
            if ( telemetry.get(7).equals("BATT") && ( Float.parseFloat( telemetry.get(5) ) > Float.parseFloat( telemetry.get(6) ) ) ) {
                ZonedDateTime epochLogDateTime = ZonedDateTime.parse(telemetry.get(0), dateTimeFormat);
                long epochLogLong = epochLogDateTime.toInstant().toEpochMilli();
                
                if (latestBattTimestamp == 0) {
                    latestBattTimestamp = epochLogLong;
                }
                else if (middleBattTimestamp == 0) {
                    middleBattTimestamp = latestBattTimestamp;
                    latestBattTimestamp = epochLogLong;
                } // If the oldestBattTimestamp is no longer relevant
                else {
                    oldestBattTimestamp = middleBattTimestamp;
                    middleBattTimestamp = latestBattTimestamp;
                    latestBattTimestamp = epochLogLong;
                }
                
                // DEBUG CODE
                //System.out.println("latestBattTimestamp: " + latestBattTimestamp);
                //System.out.println("middleBattTimestamp: " + middleBattTimestamp);
                //System.out.println("oldestBattTimestamp: " + oldestBattTimestamp);
                //System.out.println();
                
                // If we have 3 alerts within 5 minutes
                if ( (oldestBattTimestamp != 0) && ( (epochLogLong - oldestBattTimestamp) < FIVE_MINUTES) ) {
                    // DEBUG CODE
                    //System.out.println( "BATT ALARM! -- (epochLogLong - oldestBattTimestamp) = " + (epochLogLong - oldestBattTimestamp) );
                    
                    if ( (battAlerts >= 3) || (tstatAlerts >= 3) ) {
                        System.out.print(",");
                    }
                    else {
                        System.out.print("[");
                    }
                    
                    System.out.println("\n    {");
                    System.out.println("        \"satelliteId\": " + telemetry.get(1) + ",");
                    System.out.println("        \"severity\": \"RED LOW\",");
                    System.out.println("        \"component\": \"BATT\",");
                    System.out.println("        \"timestamp\": " + epochLogDateTime + "\"");
                    System.out.print("    }");
                }
                
                battAlerts++;
            } // If thermostat telemetry meets the alert threshold
            else if ( telemetry.get(7).equals("TSTAT") && ( Float.parseFloat( telemetry.get(2) ) < Float.parseFloat( telemetry.get(6) ) ) ) {
                ZonedDateTime epochLogDateTime = ZonedDateTime.parse(telemetry.get(0), dateTimeFormat);
                long epochLogLong = epochLogDateTime.toInstant().toEpochMilli();
                
                if (latestTstatTimestamp == 0) {
                    latestTstatTimestamp = epochLogLong;
                }
                else if (middleTstatTimestamp == 0) {
                    middleTstatTimestamp = latestTstatTimestamp;
                    latestTstatTimestamp = epochLogLong;
                } // If the oldestTstatTimestamp is no longer relevant
                else {
                    oldestTstatTimestamp = middleTstatTimestamp;
                    middleTstatTimestamp = latestTstatTimestamp;
                    latestTstatTimestamp = epochLogLong;
                }
                
                // DEBUG CODE
                //System.out.println("latestTstatTimestamp: " + latestTstatTimestamp);
                //System.out.println("middleTstatTimestamp: " + middleTstatTimestamp);
                //System.out.println("oldestTstatTimestamp: " + oldestTstatTimestamp);
                //System.out.println();
                
                // If we have 3 alerts within 5 minutes
                if ( (oldestTstatTimestamp != 0) && ( (epochLogLong - oldestTstatTimestamp) < FIVE_MINUTES) ) {
                    // DEBUG CODE
                    //System.out.println( "TSTAT ALARM! -- (epochLogLong - oldestTstatTimestamp) = " + (epochLogLong - oldestTstatTimestamp) );
                    
                    if ( (battAlerts >= 3) || (tstatAlerts >= 3) ) {
                        System.out.print(",");
                    }
                    else {
                        System.out.print("[");
                    }
                    
                    System.out.println("\n    {");
                    System.out.println("        \"satelliteId\": " + telemetry.get(1) + ",");
                    System.out.println("        \"severity\": \"RED HIGH\",");
                    System.out.println("        \"component\": \"TSTAT\",");
                    System.out.println("        \"timestamp\": " + epochLogDateTime + "\"");
                    System.out.print("    }");
                }
                
                tstatAlerts++;
            }
        }
        
        input.close();
        
        if ( (battAlerts > 0) || (tstatAlerts > 0) ) {
            System.out.println("\n]");
            System.out.println();
        }
        
        // DEBUG CODE
        //System.out.println("battAlerts: " + battAlerts + ", tstatAlerts: " + tstatAlerts);
    }
}
